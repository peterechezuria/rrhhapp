import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent   {

  get email() { return this.loginFormGroup.get('email'); }
  get password() { return this.loginFormGroup.get('password'); }
  loginFormGroup = this._formBuilder.group({
    email: ['', [Validators.pattern('[a-zA-Z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,3}$')]],
    password: ['', [Validators.required]],
  });
  constructor(private _formBuilder : FormBuilder) { }
}
