import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styles: [
  ]
})
export class RegistrationComponent  {

  get nombre() { return this.loginFormGroup.get('nombre'); }
  get email() { return this.loginFormGroup.get('email'); }
  get password() { return this.loginFormGroup.get('password'); }
  get confirmation() { return this.loginFormGroup.get('confirmation'); }
  loginFormGroup = this._formBuilder.group({
    nombre: ['', [Validators.required]],
    email: ['', [Validators.pattern('[a-zA-Z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,3}$'),Validators.required]],
    password: ['', [Validators.required]],
    confirmation: ['', [Validators.required]],
  });
  constructor(private _formBuilder : FormBuilder) { }

}
